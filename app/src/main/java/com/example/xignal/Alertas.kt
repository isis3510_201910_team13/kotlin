package com.example.xignal

import android.os.Bundle
import android.util.Log
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.xignal.modelo.Alerta
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import io.mapwize.mapwizeformapbox.api.LatLngFloor
import io.mapwize.mapwizeformapbox.map.Marker
import kotlinx.android.synthetic.main.alertas.*


class Alertas : AppCompatActivity() {

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alertas)
        val db = FirebaseFirestore.getInstance()
        db.collection("Mapa")
            .get()
            .addOnCompleteListener(object : OnCompleteListener<QuerySnapshot> {
                override fun onComplete(task: Task<QuerySnapshot>) {
                    if (task.isSuccessful) {
                        val valores: MutableList<Alerta> = mutableListOf()
                        for (document in task.result) {
                            val alert = Alerta(document.data.getValue("coordenadas") as LatLngFloor,
                                document.data.getValue("usuario") as String, document.data.getValue("marcador") as Marker,
                                document.data.getValue("tiempo") as Long
                            )
                            Log.d("Alertas", alert.toString())
                        }
                    } else {
                        Log.d("Alertas", "Error getting documents: ", task.exception)
                    }
                }
            })
    }
}