package com.example.xignal

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import com.example.xignal.Conectividad.LectorConexion
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.login_correo.*


class LoginCorreo : AppCompatActivity() {

    private var lectorConexion: BroadcastReceiver = LectorConexion()
    internal var mAuthListener: FirebaseAuth.AuthStateListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_correo)

        btn_login.setOnClickListener {
            val email: Editable? = txt_email.text
            val contrasena: Editable? = txt_password.text

            if( !email.isNullOrEmpty() && !contrasena.isNullOrEmpty()){
                iniciarSesion(email.toString(),contrasena.toString())
            }
            else{
                Toast.makeText(this@LoginCorreo, "Por favor ingrese los datos solicitados.", Toast.LENGTH_SHORT).show();
            }

        }
        btn_volver.setOnClickListener {
            this.finish()
        }
    }

    private fun iniciarSesion(email: String, clave: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, clave).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this@LoginCorreo, "Usuario o contraseña incorrecto. Por favor intente nuevamente.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = FirebaseAuth.getInstance().getCurrentUser()
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(lectorConexion, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(lectorConexion)
    }
}