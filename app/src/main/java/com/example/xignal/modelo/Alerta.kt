package com.example.xignal.modelo

import com.mapbox.mapboxsdk.annotations.Icon
import io.mapwize.mapwizeformapbox.api.LatLngFloor
import io.mapwize.mapwizeformapbox.map.Marker

class Alerta(coord: LatLngFloor, usu: String, mark: Marker, tiem: Long) {

    private var coordenadas: LatLngFloor = coord
    //private var icono: Icon = ico
    private var usuario: String = usu
    private var marcador: Marker = mark
    private var tiempo: Long = tiem

    fun setCoordenadas( coor : LatLngFloor){
        coordenadas = coor
    }

    fun getCoordenadas(): LatLngFloor? {
        return coordenadas
    }

    /*
    fun setIcono( ic : Icon){

        icono = ic
    }

    fun getIcono(): Icon? {
        return icono
    }
    */

    fun setUsuario( user : String){
        usuario = user
    }

    fun getUsuario(): String {
        return usuario
    }

    fun setMarcador( mar : Marker){
        marcador = mar
    }

    fun getMarcador(): Marker? {
        return marcador
    }

    fun setTiempo( time : Long){
        tiempo = time
    }

    fun getTiempo(): Long {
        return tiempo
    }
}