package com.example.xignal

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import com.example.xignal.Conectividad.LectorConexion
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.registro.*


class Registro : AppCompatActivity() {

    private var lectorConexion: BroadcastReceiver = LectorConexion()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registro)
        btn_register.setOnClickListener {
            val email: Editable? = txt_register_email.text
            val contrasena: Editable? = txt_register_password.text

            if( !email.isNullOrEmpty() && !contrasena.isNullOrEmpty()){
                registrarse(email.toString(),contrasena.toString())
            }
            else{
                Toast.makeText(this@Registro, "Por favor ingrese los datos solicitados.", Toast.LENGTH_SHORT).show();
            }

        }
        btn_cancelar.setOnClickListener {
            this.finish()
        }
    }

    private fun registrarse(email: String, clave: String) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, clave).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this@Registro, "No se pudo registrar el usuario. Por favor intente nuevamente.", Toast.LENGTH_SHORT).show();
            }
            
        }
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(lectorConexion, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(lectorConexion)
    }
}