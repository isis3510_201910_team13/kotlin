package com.example.xignal

import android.content.BroadcastReceiver
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.github.florent37.runtimepermission.kotlin.*
import kotlinx.android.synthetic.main.inicio.*
import android.content.IntentFilter
import com.example.xignal.Almacenamiento.Prefs
import com.example.xignal.Almacenamiento.SharedApp
import com.example.xignal.Conectividad.Conexion
import com.example.xignal.Conectividad.LectorConexion


class Inicio : AppCompatActivity() {

    private var lectorConexion: BroadcastReceiver = LectorConexion()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        askPermission()
        {
            setContentView(R.layout.inicio)

            SharedApp.prefs = Prefs(applicationContext)
            btn_correo.setOnClickListener {
                startActivity(Intent(this, LoginCorreo::class.java))
            }
            btn_registro.setOnClickListener {
                startActivity(Intent(this,Registro::class.java))
            }
            btn_google.setOnClickListener {
                Toast.makeText(this@Inicio, "Disponible en el sprint 3", Toast.LENGTH_SHORT).show();
            }
        }.onDeclined { e ->
            if (e.hasDenied()) {
                //appendText(resultView, "Denied :")
                e.denied.forEach {
                    //appendText(resultView, it)
                }


                AlertDialog.Builder(this@Inicio)
                    .setMessage("Por favor acepte nuestros permisos")
                    .setPositiveButton("yes") { dialog, which ->
                        e.askAgain()
                    }
                    .setNegativeButton("no") { dialog, which ->
                        dialog.dismiss()
                        e.askAgain()
                    }
                    .show()
            }

            if (e.hasForeverDenied()) {
                //appendText(resultView, "ForeverDenied :")
                e.foreverDenied.forEach {
                    //appendText(resultView, it)
                }
                e.goToSettings()
            }
        }
    }
    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(lectorConexion, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(lectorConexion)
    }
}
