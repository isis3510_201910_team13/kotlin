package com.example.xignal

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Icon
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.FragmentActivity
import android.telephony.SmsManager
import android.util.Log
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.app_bar_main.*
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton
import android.widget.ImageView
import android.widget.Toast
import com.example.xignal.Almacenamiento.SharedApp
import com.example.xignal.Conectividad.LectorConexion
import com.example.xignal.modelo.Alerta
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMapOptions
import com.mapbox.mapboxsdk.style.layers.Property
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu
import io.indoorlocation.core.IndoorLocation
import io.indoorlocation.manual.ManualIndoorLocationProvider
import io.mapwize.mapwizecomponents.ui.MapwizeFragment
import io.mapwize.mapwizeformapbox.AccountManager
import io.mapwize.mapwizecomponents.ui.MapwizeFragmentUISettings
import io.mapwize.mapwizeformapbox.api.*
import io.mapwize.mapwizeformapbox.map.MapOptions
import io.mapwize.mapwizeformapbox.map.MapwizePlugin
import io.mapwize.mapwizeformapbox.map.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity(), MapwizeFragment.OnFragmentInteractionListener {

    private var lectorConexion: BroadcastReceiver = LectorConexion()
    private var mapwizeFragment: MapwizeFragment? = null
    private var mapboxMap: MapboxMap? = null
    private var mapwizePlugin: MapwizePlugin? = null
    private var coordenadas = ArrayList<Alerta>()
    private var locationProvider: ManualIndoorLocationProvider? = null
    private var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    val MAP_CLICK = 0  // The user clicked on the map
    val PLACE_CLICK = 1// The user clicked on a place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(this, "pk.mapwize")
        setContentView(R.layout.activity_main)
        AccountManager.start(this.application, "f4d025b2bdd223e0e8584217fa64da67")

        generarMapa(savedInstanceState)

        btn_llamada.setOnClickListener {

            if(!SharedApp.prefs.number.isNullOrEmpty() && !SharedApp.prefs.msg.isNullOrEmpty() ) {
                when {
                    SharedApp.prefs.call -> {
                        if (SharedApp.prefs.sms) {
                            sendSMS()
                        }
                        call()
                    }
                    SharedApp.prefs.sms -> sendSMS()
                    else -> Toast.makeText(this@MainActivity, "Seleccione uno de los metodos de envio",Toast.LENGTH_SHORT).show()
                }
            }
            else {
                Toast.makeText(this@MainActivity, "Ingrese contacto de emergencia",Toast.LENGTH_SHORT).show()
            }
        }

        menuBoton()
        actualizarColeccion()
    }

    override fun onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    fun sendSMS()
    {
        SmsManager.getDefault().sendTextMessage(SharedApp.prefs.number, null, "Mensaje de Xignal: "+ SharedApp.prefs.msg, null, null)
    }
    fun call()
    {
        startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:${SharedApp.prefs.number}")))
    }

    fun generarMapa(savedInstanceState: Bundle?){

        val options = MapOptions.Builder()
            .restrictContentToVenue("5c9554b51ddebd00160b38ae")
            .centerOnVenue("5c9554b51ddebd00160b38ae")
            .build()

        var uiSettings = MapwizeFragmentUISettings.Builder()
            .menuButtonHidden(true)
            .compassHidden(false)
            .build()

        mapwizeFragment = MapwizeFragment.newInstance(options, uiSettings)
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        ft.add(fragmentContainer.id, mapwizeFragment!!)
        ft.commit()


    }

    fun menuBoton(){

        val itemBuilder = SubActionButton.Builder(this)
        // repeat many times:

        val itemIcon2 = ImageView(this)
        itemIcon2.setImageResource(R.drawable.bell_plus_outline)

        val itemIcon4 = ImageView(this)
        itemIcon4.setImageResource(R.drawable.settings)

        //itemBuilder.setBackgroundDrawable(getResources().getDrawable(R.color.colorVerde));
        val subActionButtonSize = 150
        val params = FrameLayout.LayoutParams(subActionButtonSize, subActionButtonSize)
        itemBuilder.setLayoutParams(params)

        val btn_alerta = itemBuilder.setContentView(itemIcon2).build()
        val btn_configuracion = itemBuilder.setContentView(itemIcon4).build()

        btn_alerta.contentDescription = "Agregar alerta"
        btn_configuracion.contentDescription = "Configuraciones"

        //attach the sub buttons to the main button
        val actionMenu = FloatingActionMenu.Builder(this)
            .addSubActionView(btn_configuracion)
            .addSubActionView(btn_alerta)
            .attachTo(btn_menu)
            .setStartAngle(0)
            .setEndAngle(90)
            .build()
        btn_alerta.setOnClickListener{
            Toast.makeText(this@MainActivity, "Alerta", Toast.LENGTH_SHORT).show()
            alertas()
        }
        btn_configuracion.setOnClickListener{
            startActivity(Intent(this, Configuraciones::class.java))
        }
    }

    /**
     * Fragment listener
     */
    override fun onFragmentReady(mapboxMap: MapboxMap?, mapwizePlugin: MapwizePlugin?) {
        this.mapboxMap = mapboxMap

        this.mapwizePlugin = mapwizePlugin

        this.locationProvider = ManualIndoorLocationProvider()
        this.mapwizePlugin?.setLocationProvider(this.locationProvider!!)

        this.mapwizePlugin?.addOnLongClickListener {
            val indoorLocation = IndoorLocation("manual_provider", it.latLngFloor.latitude, it.latLngFloor.longitude, it.latLngFloor.floor, System.currentTimeMillis())
            this.locationProvider?.setIndoorLocation(indoorLocation)
            val coor = LatLngFloor(it.latLngFloor.latitude, it.latLngFloor.longitude, it.latLngFloor.floor)
            val alert = Alerta(coor,FirebaseAuth.getInstance().currentUser.toString() ,mapwizePlugin!!.addMarker(coor), System.currentTimeMillis())
            coordenadas.add(alert)
            firestore.collection("Mapa").document(coor.toString()).set(alert)
        }

        this.mapwizePlugin?.addOnClickListener { clickEvent ->
            when(clickEvent.eventType) {
                MAP_CLICK -> {
                    if (!coordenadas.isEmpty()){
                        var arreglo = ArrayList<Alerta>()
                        for (i in 0..coordenadas.size-1){
                            if((System.currentTimeMillis() - coordenadas.get(i).getTiempo()) > 600000){
                                arreglo.add(coordenadas.get(i))
                            }else{
                                mapwizePlugin!!.addMarker(coordenadas.get(i).getCoordenadas()!!)
                            }
                        }
                        for(j in arreglo){
                            coordenadas.remove(j)
                            firestore.collection("Mapa").document(j.getCoordenadas().toString()).delete()
                            actualizarColeccion()
                        }
                    }
                }
                PLACE_CLICK -> {

                }
            }
        }

        mapwizeFragment!!.setMenuVisibility(false)

    }

    override fun onMenuButtonClick() {

    }

    override fun onInformationButtonClick(mapwizeObject: MapwizeObject?) {

    }

    override fun onFollowUserButtonClickWithoutLocation() {
        Log.i("Debug", "onFollowUserButtonClickWithoutLocation")
    }

    override fun shouldDisplayInformationButton(mapwizeObject: MapwizeObject?): Boolean {
        return false
    }

    override fun shouldDisplayFloorController(floors: MutableList<Double>?): Boolean {
        Log.i("Debug", "shouldDisplayFloorController")
        if (floors == null || floors.size <= 1) {
            return false
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(lectorConexion, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(lectorConexion)
    }

    fun alertas(){
        mapwizePlugin!!.addMarker(LatLngFloor())

    }

    fun actualizarColeccion(){
        firestore.collection("Mapa")
            .get()
            .addOnCompleteListener(object : OnCompleteListener<QuerySnapshot> {
                override fun onComplete(task: Task<QuerySnapshot>) {
                    if (task.isSuccessful) {
                        var temp: JSONObject
                        var latln: LatLngFloor
                        var alert: Alerta
                        for (document in task.result) {
                            temp = JSONObject(document.data.getValue("coordenadas").toString())
                            latln = LatLngFloor(temp.getDouble("latitude"), temp.getDouble("longitude"), temp.getDouble("floor"))
                            alert = Alerta(latln,
                                document.data.getValue("usuario") as String, mapwizePlugin!!.addMarker(latln),
                                document.data.getValue("tiempo") as Long
                            )
                            coordenadas.add(alert)
                            Log.d("Alertas", alert.toString())
                        }
                    } else {
                        Log.d("Alertas", "Error getting documents: ", task.exception)
                    }
                }
            })
    }
}

