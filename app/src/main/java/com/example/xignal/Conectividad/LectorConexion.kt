package com.example.xignal.Conectividad

import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.example.xignal.Almacenamiento.SharedApp


class LectorConexion : BroadcastReceiver() {

    var conex = Conexion()

    override fun onReceive(context: Context, intent: Intent) {

        val status = conex.getConnectivityStatusString(context)

        if( !status.equals(SharedApp.prefs.connect)){
            SharedApp.prefs.connect = status
            Toast.makeText(context, status, Toast.LENGTH_LONG).show()
        }
    }
}