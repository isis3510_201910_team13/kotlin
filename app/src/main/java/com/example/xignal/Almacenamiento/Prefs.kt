package com.example.xignal.Almacenamiento

import android.content.Context
import android.content.SharedPreferences

class Prefs (context: Context) {
    val PREFS_NAME = "com.cursokotlin.sharedpreferences"
    val SHARED_NAME = "shared_name"
    val SHARED_MSG = "shared_msg"
    val SHARED_CONTACT = "shared_contact"
    val SHARED_CONNECT = "shared_connect"
    val SHARED_CALL = "shared_call"
    val SHARED_SMS = "shared_sms"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    var number: String
        get() = prefs.getString(SHARED_NAME, "")
        set(value) = prefs.edit().putString(SHARED_NAME, value).apply()

    var msg: String
        get() = prefs.getString(SHARED_MSG, "")
        set(value) = prefs.edit().putString(SHARED_MSG, value).apply()

    var contact: String
        get() = prefs.getString(SHARED_CONTACT, "")
        set(value) = prefs.edit().putString(SHARED_CONTACT, value).apply()

    var connect: String
        get() = prefs.getString(SHARED_CONNECT, "")
        set(value) = prefs.edit().putString(SHARED_CONNECT, value).apply()

    var call: Boolean
        get() = prefs.getBoolean(SHARED_CALL,false)
        set(value) = prefs.edit().putBoolean(SHARED_CALL,value).apply()

    var sms: Boolean
        get() = prefs.getBoolean(SHARED_SMS,false)
        set(value) = prefs.edit().putBoolean(SHARED_SMS,value).apply()
}