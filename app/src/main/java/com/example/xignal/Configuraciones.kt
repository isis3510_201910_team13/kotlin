package com.example.xignal
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.*
import com.example.xignal.Almacenamiento.SharedApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.contacto_emergencia.*
import com.example.xignal.Conectividad.LectorConexion


class Configuraciones : AppCompatActivity() {

    private var btn: Button? = null
    private var menuNombre: MenuItem? = null
    private var menuTel: MenuItem? = null
    lateinit var logout: MenuItem
    private var lectorConexion: BroadcastReceiver = LectorConexion()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contacto_emergencia)

        val menu = menu_conf
        val menu1 = menu_logout
        sw_llamada.isChecked = SharedApp.prefs.call
        sw_sms.isChecked = SharedApp.prefs.sms
        menuNombre = menu.menu.findItem(R.id.itm_nombre) as MenuItem
        menuTel = menu.menu.findItem(R.id.itm_telefono) as MenuItem
        btn = findViewById(R.id.btn_elegirContacto) as Button
        logout = menu1.menu.findItem(R.id.itm_logout) as MenuItem

        menuNombre!!.title = "Nombre: " + SharedApp.prefs.contact
        menuTel!!.title = "Teléfono: " + SharedApp.prefs.number
        btn!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
            startActivityForResult(intent, 1)
        }
                val adapter = ArrayAdapter.createFromResource(this, R.array.msg_list, android.R.layout.simple_spinner_item)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spn_mensaje.adapter = adapter

        logout.setOnMenuItemClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(applicationContext, Inicio::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            true
        }
    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val contactData = data!!.data
            val c = contentResolver.query(contactData!!, null, null, null, null)
            if (c!!.moveToFirst()) {

                var phoneNumber = ""
                val name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))
                var hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                if (hasPhone.equals("1", ignoreCase = true))
                    hasPhone = "true"
                else
                    hasPhone = "false"

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null)
                    while (phones!!.moveToNext()) {
                        phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    }
                    phones.close()
                }

                SharedApp.prefs.number = phoneNumber
                SharedApp.prefs.contact = name
                menuNombre!!.title = "Nombre: " + SharedApp.prefs.contact
                menuTel!!.title = "Teléfono: " + SharedApp.prefs.number
            }
            c.close()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        SharedApp.prefs.msg = spn_mensaje.selectedItem.toString()
        SharedApp.prefs.call = sw_llamada.isChecked
        SharedApp.prefs.sms = sw_sms.isChecked
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(lectorConexion, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(lectorConexion)
    }
}